//
//  SplashController.h
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/8/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import "CheckStockViewController.h"

@interface SplashViewController : UIViewController <UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSMutableArray *countryNames;
    NSMutableArray *countryValues;
    
    IBOutlet UIPickerView *pickerView;
}

@end
