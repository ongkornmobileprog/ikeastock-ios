//
//  AppDelegate.h
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/3/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SplashViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
//NSObject <UIApplicationDelegate>
{
    
    SplashViewController *mviewController;
    
    UIWindow *window;
}
//@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) SplashViewController *mviewController;

@end

/////////