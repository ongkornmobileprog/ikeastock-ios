//
//  ProductDBHelper.h
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/10/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface ProductDBHelper : NSObject
{
    NSString *databasePath;
}

+(ProductDBHelper*) getSharedInstance;
-(BOOL) createDB;
-(NSMutableArray*) getAllProductInDB;
-(NSInteger) getIDInDB:(NSString*)productNum;
-(BOOL) addProductToDB:(NSString*)productNum targetStock:(NSInteger)targetStock country:(NSString*)selectedCountryParam;
-(BOOL) deleteProductFromDB:(NSString*)productNum;
-(void) clearProductDB;

@end
