//
//  SplashController.m
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/8/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import "SplashViewController.h"


@interface SplashViewController ()

@property NSString *countrySendValue;

@end

@implementation SplashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    countryNames = [[NSMutableArray alloc] init];
    [countryNames addObject:@"- - -"];
    [countryNames addObject:@"TH"];
    [countryNames addObject:@"JP"];
    [countryNames addObject:@"US"];
    [countryNames addObject:@"SE"];
    [countryNames addObject:@"FI"];
    [countryNames addObject:@"GB"];
    [countryNames addObject:@"SG"];
    [countryNames addObject:@"KR"];
    
    countryValues = [[NSMutableArray alloc] init];
    [countryValues addObject:@"N/A"];
    [countryValues addObject:@"th/th"];
    [countryValues addObject:@"jp/ja"];
    [countryValues addObject:@"us/en"];
    [countryValues addObject:@"se/sv"];
    [countryValues addObject:@"fi/fi"];
    [countryValues addObject:@"gb/en"];
    [countryValues addObject:@"sg/en"];
    [countryValues addObject:@"kr/ko"];
    
    [pickerView selectRow:1 inComponent:0 animated:NO];
    self.countrySendValue = [countryValues objectAtIndex:[pickerView selectedRowInComponent:0]];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/////////////////////////////   Test Noti Btn   /////////////////////////////////////
- (IBAction)TestNoti:(id)sender {
    NSLog(@"Test Notification");
    
    UILocalNotification *localNotification = [[UILocalNotification alloc] init];
    localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:10];
    localNotification.alertBody = [NSString stringWithFormat:@"Test Noti"];
    localNotification.soundName = UILocalNotificationDefaultSoundName;
    localNotification.applicationIconBadgeNumber = 1;
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

/////////////////////////////   TableView   /////////////////////////////////////

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;
{
    return 1;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.countrySendValue = [countryValues objectAtIndex:row];
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component;
{
    return [countryNames count];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, pickerView.frame.size.width, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor whiteColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text  = [countryNames objectAtIndex:row];
    return label;
}

#pragma mark - Navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"to_main"]) {
        CheckStockViewController *target = [segue destinationViewController];
        NSLog(@"%@", self.countrySendValue);
        target.selectedCountry = self.countrySendValue;
    }
}

@end
