//
//  Product.m
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/13/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import "Product.h"
#import "GDataXMLNode.h"

#define xPathLocalStores @"//ir:ikea-rest/availability/localStore"
#define kBuCode @"buCode"
#define kStock @"stock"
#define kAvailableStock @"availableStock"
#define kInStockProbCode @"inStockProbabilityCode"

@interface Product ()

@property NSString *IKEA_PREFIX_XML_1;
@property NSString *IKEA_PREFIX_XML_2;
@property NSString *IKEA_PREFIX_VIEW_PRODUCT_1;
@property NSString *IKEA_PREFIX_VIEW_PRODUCT_2;

@end

@implementation Product

/*
 getUpdateMaxStockAndStore
 return NSArray index 0 -> maxAvailableStockStr
                            -> maxAvailableStockStr == ""  -> 0/1
                index 1 -> maxAvailableStockStr
                            -> maxLocalStore        == ""  -> N/A
 */
+ (NSArray*) getUpdateMaxStockAndStore:(NSString*)productNumParam country:(NSString*)selectedCountryParam {
    
    NSString *IKEA_PREFIX_XML_1 = @"http://www.ikea.com/";
    NSString *IKEA_PREFIX_XML_2 = @"/iows/catalog/availability/";
    
    NSInteger maxAvailableStockInt = 0;
    NSString *maxAvailableStockStr = @"";
    NSString *maxLocalStore = @"N/A";
    
    NSString *stPREFIX_URL_XML =  [NSString stringWithFormat:@"%@%@%@", IKEA_PREFIX_XML_1, selectedCountryParam, IKEA_PREFIX_XML_2];
    stPREFIX_URL_XML = [stPREFIX_URL_XML stringByAppendingString:productNumParam];
    NSURL *URL=[NSURL URLWithString:stPREFIX_URL_XML];
    NSData *contentData = [NSData dataWithContentsOfURL:URL];
    
    NSError *error = nil;

    // GDataXML
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:contentData options:0 error:&error];
    NSArray *localStores = [doc nodesForXPath:xPathLocalStores error:&error];
    NSMutableArray *array = [NSMutableArray array];
    
    for (GDataXMLElement *localStore in localStores) {

        GDataXMLNode *localStoreXMLNode = [localStore attributeForName:kBuCode];
        NSString *buCode = [(GDataXMLElement *) localStoreXMLNode stringValue];
        GDataXMLElement *stockElement = (GDataXMLElement *)[localStore elementsForName:kStock][0];
        GDataXMLElement *availableStockElement = (GDataXMLElement *)[stockElement elementsForName:kAvailableStock][0];
        NSString *availableStock = [availableStockElement stringValue];
        NSInteger availableStockInt =[availableStock intValue];
    
        if (availableStockInt > maxAvailableStockInt) {
            maxAvailableStockInt = availableStockInt;
            maxAvailableStockStr = [NSString stringWithFormat:@"%ld", (long)maxAvailableStockInt];
            maxLocalStore = buCode;
        }
        
    }
    [array addObject:maxAvailableStockStr];
    [array addObject:maxLocalStore];
   
    return array;
}

@end
