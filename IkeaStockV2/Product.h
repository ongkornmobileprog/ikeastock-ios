//
//  Product.h
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/13/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

+(NSArray*) getUpdateMaxStockAndStore:(NSString*)productNumParam country:(NSString*)selected_country;

@end
