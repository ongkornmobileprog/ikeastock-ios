//
//  TrackingViewController.m
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/11/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import "TrackingViewController.h"
#import "ProductDBHelper.h"

#define xPathLocalStores @"//ir:ikea-rest/availability/localStore"
#define kBuCode @"buCode"
#define kStock @"stock"
#define kAvailableStock @"availableStock"
#define kInStockProbCode @"inStockProbabilityCode"

#define kProductNum @"productNum"
#define kTargetStock @"targetStock"
#define kSelectedCountry @"selectedCountry"

@interface TrackingViewController ()

@property NSString *IKEA_PREFIX_XML_1;
@property NSString *IKEA_PREFIX_XML_2;
@property NSString *IKEA_PREFIX_VIEW_PRODUCT_1;
@property NSString *IKEA_PREFIX_VIEW_PRODUCT_2;
@property (strong, nonatomic) IBOutlet UITableView *trackingProductTableView;

@end

@implementation TrackingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setTitle:@"Tracking Product List"];
    
    //[stockTableView setHidden:YES];
    [self.start_track_button setHidden:YES];
    [self.stop_track_button setHidden:YES];
    
    self.IKEA_PREFIX_XML_1 = @"http://www.ikea.com/";
    self.IKEA_PREFIX_XML_2 = @"/iows/catalog/availability/";
    self.IKEA_PREFIX_VIEW_PRODUCT_1 = @"http://www.ikea.com/";
    self.IKEA_PREFIX_VIEW_PRODUCT_2 = @"/catalog/products/";
    
    tableData = self.receiveDataArray;
    NSLog(@"tableData:%lu", (unsigned long)[tableData count]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/////////////////////////////   TableView   /////////////////////////////////////
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
    //return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *stockTableIdentifier = @"stockingProductCell";
    
    UITableViewCell *cell = [stockTableView dequeueReusableCellWithIdentifier:stockTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:stockTableIdentifier];
    }
    
    NSString *buCodeStrTV = [[tableData objectAtIndex:indexPath.row] valueForKey:kBuCode];
    NSString *availableStockStrTV = [[tableData objectAtIndex:indexPath.row] valueForKey:kAvailableStock];
    NSString *targetStockStrTV = [[tableData objectAtIndex:indexPath.row] valueForKey:kTargetStock];
    NSString *productNumTV = [[tableData objectAtIndex:indexPath.row] valueForKey:kProductNum];
    
    
    if ([availableStockStrTV  isEqual: @""]) {
        availableStockStrTV = @"0";
    }
    if ([buCodeStrTV  isEqual: @""]) {
        buCodeStrTV = @"N/A";
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Product Number: %@", productNumTV];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Stock: %@/%@\rStore Number: %@", availableStockStrTV, targetStockStrTV, buCodeStrTV];
    
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.numberOfLines = 2;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *productNumTV = [[tableData objectAtIndex:indexPath.row] valueForKey:kProductNum];
    NSString *selectedCountryTV = [[tableData objectAtIndex:indexPath.row] valueForKey:kSelectedCountry];
    NSString *itemURL =  [NSString stringWithFormat:@"%@%@%@", self.IKEA_PREFIX_VIEW_PRODUCT_1, selectedCountryTV, self.IKEA_PREFIX_VIEW_PRODUCT_2];
    itemURL = [itemURL stringByAppendingString:productNumTV];
    
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:itemURL]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:itemURL]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"URL error" message:[NSString stringWithFormat:@"No custom URL defined for %@", itemURL] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
    
}
//////////////////////////////////////////////////////////////////

- (IBAction)start_tracking:(id)sender {
    [self.stop_track_button setHidden:NO];
    [self.start_track_button setHidden:YES];
}

- (IBAction)stop_trakcing:(id)sender {
    [self.stop_track_button setHidden:YES];
    [self.start_track_button setHidden:NO];
}

- (IBAction)clear_db_btn:(id)sender {
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:@"Clear All Tracking Products"
                                  message:@"Clear all products in tracking list. Are you sure ?"
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction
                         actionWithTitle:@"Clear"
                         style:UIAlertActionStyleDefault
                         handler:^(UIAlertAction * action)
                         {
                             [alert dismissViewControllerAnimated:YES completion:nil];
                             [[ProductDBHelper getSharedInstance] clearProductDB];
                             tableData = [[ProductDBHelper getSharedInstance] getAllProductInDB];
                             [_trackingProductTableView reloadData];
                             
                         }];
    UIAlertAction* cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleDefault
                             handler:^(UIAlertAction * action)
                             {
                                 [alert dismissViewControllerAnimated:YES completion:nil];
                             }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:nil];
}

@end
