//
//  ViewController.h
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/3/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CheckStockViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *tableData;
    IBOutlet UITableView *stockTableView;
}
@property (strong, nonatomic) IBOutlet UITextField *product_id_edittext;
@property (strong, nonatomic) IBOutlet UIButton *open_ikea;
@property (strong, nonatomic) IBOutlet UIButton *find_stock;
@property (strong, nonatomic) IBOutlet UIButton *monitor_product;
@property (strong, nonatomic) IBOutlet UIButton *all_track_products;

@property (retain, nonatomic) NSString *selectedCountry;
@property (strong, nonatomic) NSString *productNum;

- (IBAction)open_ikea_method:(id)sender;
- (IBAction)viewProduct:(id)sender;
+ (NSMutableArray*)fetchDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler;


@end

