//
//  main.m
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/3/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
