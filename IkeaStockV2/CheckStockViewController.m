//
//  ViewController.m
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/3/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import "CheckStockViewController.h"
#import "SplashViewController.h"
#import "TrackingViewController.h"
#import "ProductDBHelper.h"
#import "GDataXMLNode.h"
#import "Product.h"

#define xPathLocalStores @"//ir:ikea-rest/availability/localStore"
#define kBuCode @"buCode"
#define kStock @"stock"
#define kAvailableStock @"availableStock"
#define kInStockProbCode @"inStockProbabilityCode"

#define kProductNum @"productNum"
#define kTargetStock @"targetStock"
#define kSelectedCountry @"selectedCountry"


#define noDataInTableView @"No data is currently available.."
#define fillProductNumber @"Fill product number\rAnd press find stock"

@interface CheckStockViewController ()

@property NSString *IKEA_PREFIX_XML_1;
@property NSString *IKEA_PREFIX_XML_2;
@property NSString *IKEA_PREFIX_VIEW_PRODUCT_1;
@property NSString *IKEA_PREFIX_VIEW_PRODUCT_2;
@property NSInteger numOfLocalStores;
@property NSInteger numOfAvailableStock;
@property Boolean isFind;


@end

@implementation CheckStockViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isFind = NO;
    //[stockTableView setHidden:YES];
    [self.monitor_product setHidden:YES];
    self.numOfLocalStores = 0;
    NSLog(@"Select -> %@", self.selectedCountry);
    
    [self.navigationItem setHidesBackButton:YES];
    //[super viewDidLoad];
    
    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
                                   initWithTitle:@"Change Country"
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(ChangeCountry:)];
    self.navigationItem.rightBarButtonItem = flipButton;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.IKEA_PREFIX_XML_1 = @"http://www.ikea.com/";
    self.IKEA_PREFIX_XML_2 = @"/iows/catalog/availability/";
    self.IKEA_PREFIX_VIEW_PRODUCT_1 = @"http://m.ikea.com/";
    self.IKEA_PREFIX_VIEW_PRODUCT_2 = @"/catalog/products/art/";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)dismissKeyboard {
    [_product_id_edittext resignFirstResponder];
}


/////////////////////////////   Change Country Button   /////////////////////////////////////
-(IBAction)ChangeCountry:(id)sender
{
    [self performSegueWithIdentifier:@"reset" sender:self];
}

/////////////////////////////   WEB IKEA Button   /////////////////////////////////////
- (IBAction)open_ikea_method:(id)sender {
    NSString *ikea_url = [NSString alloc];
    
    if([self.product_id_edittext.text length] == 0){
        ikea_url = [NSString stringWithFormat:@"%@%@/", self.IKEA_PREFIX_XML_1, self.selectedCountry];
    } else if ([self.product_id_edittext.text length] > 0) {
        ikea_url = [NSString stringWithFormat:@"%@%@%@%@/", self.IKEA_PREFIX_VIEW_PRODUCT_1, self.selectedCountry, self.IKEA_PREFIX_VIEW_PRODUCT_2, self.product_id_edittext.text];
    }
    
    if([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:ikea_url]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ikea_url]];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"URL error" message:[NSString stringWithFormat:@"No custom URL defined for %@", ikea_url] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
    }
}

/////////////////////////////   VIEW PRODUCT Button   /////////////////////////////////////
- (IBAction)viewProduct:(id)sender {
    self.numOfAvailableStock = 0;
    self.numOfLocalStores = 0;
    self.isFind = YES;
    
    NSString *stPREFIX_URL_XML =  [NSString stringWithFormat:@"%@%@%@", self.IKEA_PREFIX_XML_1, self.selectedCountry, self.IKEA_PREFIX_XML_2];
    self.productNum = self.product_id_edittext.text;
    stPREFIX_URL_XML = [stPREFIX_URL_XML stringByAppendingString:self.productNum];
    NSURL *URL=[NSURL URLWithString:stPREFIX_URL_XML];
    NSData *contentData = [NSData dataWithContentsOfURL:URL];
    
    NSError *error = nil;
    
    //NSString *theXML = [NSString stringWithContentsOfURL:URL encoding:NSUTF8StringEncoding error:&error]; // Unused
    
    // GDataXML
    GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:contentData options:0 error:&error];
    NSArray *localStores = [doc nodesForXPath:xPathLocalStores error:&error];
    NSMutableArray *array = [NSMutableArray array];
    
    self.numOfAvailableStock = 0;
    
    for (GDataXMLElement *localStore in localStores) {
        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
        
        GDataXMLNode *localStoreXMLNode = [localStore attributeForName:kBuCode];
        
        // buCode
        NSString *buCode = [(GDataXMLElement *) localStoreXMLNode stringValue];
        
        GDataXMLElement *stockElement = (GDataXMLElement *)[localStore elementsForName:kStock][0];
        
        // AvailableStock
        GDataXMLElement *availableStockElement = (GDataXMLElement *)[stockElement elementsForName:kAvailableStock][0];
        NSString *availableStock = [availableStockElement stringValue];
        NSInteger availableStockInt = [availableStock intValue];
        if (availableStockInt > 0) {
            self.numOfAvailableStock += availableStockInt;
        }
        
        // inStockProbCode
        GDataXMLElement *inStockProbCodeElement = (GDataXMLElement *)[stockElement elementsForName:kInStockProbCode][0];
        NSString *inStockProbCode = [inStockProbCodeElement stringValue];
        
        [dict setObject:buCode forKey:kBuCode];
        [dict setObject:availableStock forKey:kAvailableStock];
        [dict setObject:inStockProbCode forKey:kInStockProbCode];
        
        [array addObject:dict];
    }
    
    tableData = array;
    self.numOfLocalStores = [localStores count];
    [stockTableView reloadData];
    [stockTableView setHidden:NO];
    
    NSLog(@"stock: %ld", (long)self.numOfAvailableStock);
    
    if (self.numOfAvailableStock == 0) {
        [self.monitor_product setHidden:NO];
    } else {
        //TODO for debug, always show
        [self.monitor_product setHidden:NO];
    }
}

/////////////////////////////   TableView   /////////////////////////////////////
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView{
    if (self.isFind == YES) {
        stockTableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        stockTableView.backgroundView = nil;
        return 1;
    } else {
        UILabel *messageLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, self.view.bounds.size.height)];
        
        messageLabel.text = fillProductNumber;
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.numberOfLines = 0;
        messageLabel.textAlignment = NSTextAlignmentCenter;
        messageLabel.font = [UIFont fontWithName:@"Palatino-Italic" size:18];
        [messageLabel sizeToFit];
        
        stockTableView.backgroundView = messageLabel;
        stockTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //return [self.tableData count];
    return self.numOfLocalStores;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *stockTableIdentifier = @"stockTableItem";
    UITableViewCell *cell = [stockTableView dequeueReusableCellWithIdentifier:stockTableIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:stockTableIdentifier];
    }
    
    NSString *buCodeStr = [[tableData objectAtIndex:indexPath.row] valueForKey:kBuCode];
    NSString *availableStockStr = [[tableData objectAtIndex:indexPath.row] valueForKey:kAvailableStock];
    NSString *inStockProbCodeStr = [[tableData objectAtIndex:indexPath.row] valueForKey:kInStockProbCode];
    
    cell.textLabel.text = [NSString stringWithFormat:@"-- Store Number: %@ --", buCodeStr];
    cell.detailTextLabel.text = [NSString stringWithFormat:@"Stock: %@\rStock Probability: %@", availableStockStr, inStockProbCodeStr];
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.numberOfLines = 2;
    return cell;
}

/////////////////////////////   prepareForSegue   /////////////////////////////////////
#pragma mark - Navigation
- (void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    /////////////////////////////   MONITOR THIS PRODUCT Button   /////////////////////////////////////
    if ([segue.identifier isEqualToString:@"addTrackingProduct"]) {
        TrackingViewController *target = [segue destinationViewController];
        BOOL isSaveProduct = NO;
        NSString *alertString = @"This product is already in tracking list.";
        
        //isSaveProduct = [[ProductDBHelper getSharedInstance] addProductToDB:self.productNum targetStock:1];
        isSaveProduct = [[ProductDBHelper getSharedInstance] addProductToDB:self.productNum targetStock:1 country:self.selectedCountry];

        if (isSaveProduct == NO) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:alertString message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }

        NSMutableArray *allProductArray = [[NSMutableArray alloc] init];
        NSMutableArray *dataFromDB = [[ProductDBHelper getSharedInstance] getAllProductInDB];
        
        if (dataFromDB == nil) {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Data not found" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else{
            
            for (NSInteger i = 0; i < [dataFromDB count]; i++) {
                NSMutableDictionary *detailStockDict = [[NSMutableDictionary alloc] init];
                //NSString *idTemp = [[dataFromDB objectAtIndex:i] valueForKey:@"id"];                    // Unused
                NSString *productNumTemp = [[dataFromDB objectAtIndex:i] valueForKey:kProductNum];    // xxxxx
                NSString *targetStockTemp = [[dataFromDB objectAtIndex:i] valueForKey:kTargetStock];  // 1
                
                NSArray *getMaxStock = [Product getUpdateMaxStockAndStore:productNumTemp country:self.selectedCountry];
                
                NSString * maxAvailableStockStr = [getMaxStock objectAtIndex:0];
                NSString * maxLocalStore = [getMaxStock objectAtIndex:1];
                
                
                [detailStockDict setObject:productNumTemp forKey:kProductNum];
                [detailStockDict setObject:targetStockTemp forKey:kTargetStock];
                [detailStockDict setObject:maxAvailableStockStr forKey:kAvailableStock];
                [detailStockDict setObject:maxLocalStore forKey:kBuCode];
                [detailStockDict setObject:self.selectedCountry forKey:kSelectedCountry];
                
                [allProductArray addObject:detailStockDict];
            }
        }
        target.receiveDataArray = allProductArray;
        
    } else if ([segue.identifier isEqualToString:@"viewTrackingProduct"]){
        //////////////////////////   VIEW TRACKING PRODUCTS Button   //////////////////////////////////
        TrackingViewController *target = [segue destinationViewController];
        NSMutableArray *allProductArray = [[NSMutableArray alloc] init];
        NSMutableArray *dataFromDB = [[ProductDBHelper getSharedInstance] getAllProductInDB];

        for (NSInteger i = 0; i < [dataFromDB count]; i++) {
            NSMutableDictionary *detailStockDict = [[NSMutableDictionary alloc] init];
            //NSString *idTemp = [[dataFromDB objectAtIndex:i] valueForKey:@"id"];                    // Unused
            NSString *productNumTemp = [[dataFromDB objectAtIndex:i] valueForKey:kProductNum];    // xxxxx
            NSString *targetStockTemp = [[dataFromDB objectAtIndex:i] valueForKey:kTargetStock];  // 1
                
            NSArray *getMaxStock = [Product getUpdateMaxStockAndStore:productNumTemp country:self.selectedCountry];
                
            NSString * maxAvailableStockStr = [getMaxStock objectAtIndex:0];
            NSString * maxLocalStore = [getMaxStock objectAtIndex:1];
                
                
            [detailStockDict setObject:productNumTemp forKey:kProductNum];
            [detailStockDict setObject:targetStockTemp forKey:kTargetStock];
            [detailStockDict setObject:maxAvailableStockStr forKey:kAvailableStock];
            [detailStockDict setObject:maxLocalStore forKey:kBuCode];
            [detailStockDict setObject:self.selectedCountry forKey:kSelectedCountry];
            
            [allProductArray addObject:detailStockDict];
        }
        target.receiveDataArray = allProductArray;
    } else if ([segue.identifier isEqualToString:@"reset"]){
        /////////////////////////////   CHANGE COUNTRY Button   /////////////////////////////////////
        [[ProductDBHelper getSharedInstance] clearProductDB];
    }
    
}

/////////////////////// fetchDataWithCompletionHandler ///////////////////////
+ (NSMutableArray*)fetchDataWithCompletionHandler:(void (^)(UIBackgroundFetchResult)) completionHandler {
    
    NSLog(@"fetchDataWithCompletionHandler");
    
    NSMutableArray *allProductArray = [[NSMutableArray alloc] init];
    NSMutableArray *dataFromDB = [[ProductDBHelper getSharedInstance] getAllProductInDB];
    
    for (NSInteger i = 0; i < [dataFromDB count]; i++) {
        NSMutableDictionary *detailStockDict = [[NSMutableDictionary alloc] init];
        NSString *productNumTemp = [[dataFromDB objectAtIndex:i] valueForKey:kProductNum];    // xxxxx
        NSString *selectedCountryTemp = [[dataFromDB objectAtIndex:i] valueForKey:kSelectedCountry];
        
        NSArray *getMaxStock = [Product getUpdateMaxStockAndStore:productNumTemp country:selectedCountryTemp];
        
        NSString * maxAvailableStockStr = [getMaxStock objectAtIndex:0];
        NSInteger maxAvailableStockInt = [maxAvailableStockStr integerValue];
        NSString * maxLocalStore = [getMaxStock objectAtIndex:1];
        
        if (maxAvailableStockInt > 0) {
            [detailStockDict setObject:productNumTemp forKey:kProductNum];
            [detailStockDict setObject:maxAvailableStockStr forKey:kAvailableStock];
            [detailStockDict setObject:maxLocalStore forKey:kBuCode];
            
            [allProductArray addObject:detailStockDict];
        }
    }
    return allProductArray;
}


@end
