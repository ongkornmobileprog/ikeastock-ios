//
//  TrackingViewController.h
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/11/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TrackingViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *tableData;
    IBOutlet UITableView *stockTableView;
}

@property (strong, nonatomic) NSMutableArray *receiveDataArray;
@property (strong, nonatomic) NSString *selected_country;
//@property (strong, nonatomic) NSString *productNum;
@property (strong, nonatomic) IBOutlet UIButton *start_track_button;
@property (strong, nonatomic) IBOutlet UIButton *stop_track_button;

- (IBAction)start_tracking:(id)sender;
- (IBAction)stop_trakcing:(id)sender;

@end
