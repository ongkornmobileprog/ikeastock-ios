//
//  AppDelegate.m
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/3/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import "AppDelegate.h"
#import "SplashViewController.h"
#import "ProductDBHelper.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

@synthesize window;
@synthesize mviewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [launchOptions valueForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
    {
        
        [application registerUserNotificationSettings:[UIUserNotificationSettings
                                                       settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|
                                                       UIUserNotificationTypeSound categories:nil]];
    }
    
    [application setMinimumBackgroundFetchInterval:UIApplicationBackgroundFetchIntervalMinimum];

    return YES;
}


//////////

- (void)applicationDidFinishLaunching:(UIApplication *)application
{
    mviewController = [[SplashViewController alloc] initWithNibName:@"ViewController" bundle:[NSBundle mainBundle]];
    [window addSubview:mviewController.view];
    [window makeKeyAndVisible];
}

-(void)application:(UIApplication *)application performFetchWithCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    NSLog(@"Fetch started");
    
    UIBackgroundFetchResult fetchResult = UIBackgroundFetchResultFailed;
    
    NSInteger trackingProductCount = [[[ProductDBHelper getSharedInstance] getAllProductInDB] count];
    
    if(trackingProductCount > 0){
        
        NSMutableArray *allProductArray = [CheckStockViewController fetchDataWithCompletionHandler:completionHandler];
        
        if ([allProductArray count] > 0) {
            
            for (NSInteger i = 0; i < [allProductArray count]; i++) {
                NSString * productNum = [[allProductArray objectAtIndex:i] valueForKey:@"productNum"];
                NSString * storeNum = [[allProductArray objectAtIndex:i] valueForKey:@"buCode"];
                
                NSLog(@"Product %@ is in-stock at Store: %@", productNum, storeNum);
                
                //send local notification
                
                NSLog(@"send local notification (%ld)", (long)i);
                
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
                localNotification.alertBody = [NSString stringWithFormat:@"Product %@ is in-stock at Store: %@", productNum, storeNum];
                localNotification.soundName = UILocalNotificationDefaultSoundName;
                localNotification.applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
                
                NSDictionary *extraInfo = @{@"productNum" : productNum};
                localNotification.userInfo = extraInfo;
                
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
            fetchResult = UIBackgroundFetchResultNewData;
            
        } else {
            
            NSLog(@"All product out of stock, keep tracking ...");
            
            UILocalNotification *localNotification = [[UILocalNotification alloc] init];
            localNotification.fireDate = [NSDate dateWithTimeIntervalSinceNow:0];
            localNotification.alertBody = @"All product out of stock. Keep tracking ...";
            localNotification.soundName = UILocalNotificationDefaultSoundName;
            localNotification.applicationIconBadgeNumber = 0;
            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            
            fetchResult = UIBackgroundFetchResultNoData;
            
        }
        
    } else {
    
        NSLog(@"No product in tracking list.");
        fetchResult = UIBackgroundFetchResultNoData;
        
    }
    
    completionHandler(fetchResult);
    NSLog(@"Fetch completed");
    
    //TrackingViewController *fetcher = [TrackingViewController sharedManager];
    
    /*
     if ([fetcher hasOutstandingFetch]) {
     fetchResult = UIBackgroundFetchResultNoData;
     }
     
     else if ([fetcher hasDataPendingProcessing]) {
     [self processData:fetcher.processingData]l
     [fetcher markFetchProcessed];
     
     fetchResult = UIBackgroundFetchResultNewData;
     }
     
     else {
     [fetcher beginFetch];
     fetchResult = UIBackgroundFetchResultNoData;
     }
     */
    
    
    /*
     NSUserDefaults *standardDefaults = [NSUserDefaults standardUserDefaults];
     
     // Get Current Location from NSUserDefaults
     CLLocationCoordinate2D currentLocation;
     currentLocation.latitude = [standardDefaults floatForKey:@"locationLatitude"];
     currentLocation.longitude = [standardDefaults floatForKey:@"locationLongitude"];
     
     // GetWeather for current location
     GetWeather *getWeather = [[GetWeather alloc] init];
     [getWeather getWeatherAtCurrentLocation:currentLocation];
     
     // Set up Local Notifications
     [[UIApplication sharedApplication] cancelAllLocalNotifications];
     UILocalNotification *localNotification = [[UILocalNotification alloc] init];
     NSDate *now = [NSDate date];
     localNotification.fireDate = now;
     localNotification.alertBody = [NSString stringWithFormat:@"Temperature in %@ is %@.", getWeather.currentLocation, getWeather.currentTemperature];
     localNotification.soundName = UILocalNotificationDefaultSoundName;
     localNotification.applicationIconBadgeNumber = [getWeather.currentTemperature intValue];
     [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
     */
    //[TrackingViewController test_mthod];
    
    //completionHandler(fetchResult);
    //[CheckStockViewController fetchDataWithCompletionHandler:completionHandler];
    
}

/*
- (void)dealloc
{
    [window release];
    [SplashController release];
    [super dealloc];
}
*/
//////////

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIAlertView *notificationAlert = [[UIAlertView alloc]
                                      initWithTitle:@"Item in stock!"
                                      message:notification.alertBody
                                      delegate:self
                                      cancelButtonTitle:@"OK"
                                      otherButtonTitles:nil, nil];
    
    [notificationAlert show];
    
    NSString *productNum = [notification.userInfo objectForKey:@"productNum"];
    
    if ([[ProductDBHelper getSharedInstance] deleteProductFromDB:productNum]){
        NSLog(@"Product %@ is deleted from DB", productNum);
    } else {
        NSLog(@"ERROR: Product %@ is NOT deleted from DB", productNum);
    }
    
    // Set icon badge number to zero
    application.applicationIconBadgeNumber = 0;
}

@end
