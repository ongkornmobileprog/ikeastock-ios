//
//  ProductDBHelper.m
//  IkeaStockV2
//
//  Created by Tanaka Paorat on 5/10/2559 BE.
//  Copyright (c) 2559 Tanaka Paorat. All rights reserved.
//

#import "ProductDBHelper.h"

static ProductDBHelper *sharedInstance = nil;
static sqlite3 *database = nil;
static sqlite3_stmt *statement = nil;

@implementation ProductDBHelper

/////////////////////////////   getSharedInstance   /////////////////////////////////////
+ (ProductDBHelper*) getSharedInstance{
    if (!sharedInstance) {
        sharedInstance = [[super allocWithZone:NULL]init];
        [sharedInstance createDB];
    }
    return sharedInstance;
}

/*
 TrackingProduct.db
 products TABLE
 
 id INTEGER PRIMARY KEY AUTOINCREMENT
 productNum TEXT
 targetStock INTEGER
 selectedCountry TEXT
 */
/////////////////////////////   createDB   /////////////////////////////////////
- (BOOL) createDB{
    NSString *docsDir;
    NSArray *dirPaths;
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"TrackingProduct.db"]];
    
    BOOL isSuccess = YES;
    NSFileManager *filemgr = [NSFileManager defaultManager];
    if ([filemgr fileExistsAtPath: databasePath ] == NO) {
        const char *dbpath = [databasePath UTF8String];
        if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
            char *errMsg;
            const char *sql_stmt = "CREATE TABLE IF NOT EXISTS products (id INTEGER PRIMARY KEY AUTOINCREMENT, productNum TEXT, targetStock INTEGER, selectedCountry TEXT)";
            
            if (sqlite3_exec(database, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK)
            {
                isSuccess = NO;
                NSLog(@"Failed to create table");
            }
            sqlite3_close(database);
            return  isSuccess;
        } else {
            isSuccess = NO;
            NSLog(@"Failed to open/create database");
        }
    }
    return isSuccess;
}

/////////////////////////////   getAllProductInDB   /////////////////////////////////////
- (NSMutableArray*) getAllProductInDB {
    NSLog(@"getAllProductInDB");
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM products ORDER BY id"];
        const char *query_stmt = [querySQL UTF8String];
        NSMutableArray *resultArray = [[NSMutableArray alloc] init];
        
        NSInteger indexInt;
        NSString *indexStr;
        NSString *productNum;
        NSInteger targetStockInt;
        NSString *targetStockStr;
        NSString *selectedCountry;
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                NSMutableDictionary *resultDict = [NSMutableDictionary dictionary];
                indexInt = sqlite3_column_int(statement, 0);
                indexStr = [NSString stringWithFormat:@"%ld", (long)indexInt];
                productNum = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                targetStockInt = sqlite3_column_int(statement, 2);
                targetStockStr = [NSString stringWithFormat:@"%ld", (long)targetStockInt];
                selectedCountry  = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 3)];
                
                [resultDict setObject:indexStr forKey:@"id"];
                [resultDict setObject:productNum forKey:@"productNum"];
                [resultDict setObject:targetStockStr forKey:@"targetStock"];
                [resultDict setObject:selectedCountry forKey:@"selectedCountry"];
                [resultArray addObject:resultDict];
            }
            sqlite3_finalize(statement);
            return resultArray;
        } else {
            NSLog(@"Not found");
            sqlite3_reset(statement);
            return nil;
        }
        
    }
    return nil;
}

/////////////////////////////   getIDInDB   /////////////////////////////////////
- (NSInteger) getIDInDB:(NSString*)productNum{
    NSLog(@"getIDInDB(%@)", productNum);
    
    NSInteger index = -1;
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM products ORDER BY productNum"];
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(database, query_stmt, -1, &statement, NULL) == SQLITE_OK) {
            while (sqlite3_step(statement) == SQLITE_ROW) {
                if (productNum == [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)]) {
                    index = sqlite3_column_int(statement, 0);
                    sqlite3_reset(statement);
                    return index;
                }
            }
        } else {
            NSLog(@"Can't read DB");
            sqlite3_reset(statement);
            return -1;
        }
    }
    return index;
}

/////////////////////////////   addProductToDB   /////////////////////////////////////
- (BOOL) addProductToDB:(NSString*)productNum targetStock:(NSInteger)targetStock country:(NSString*)selectedCountryParam {
    NSLog(@"addProductToDB(%@)", productNum);
    
    NSInteger indexInDB =[self getIDInDB:productNum];
    if(indexInDB != -1){
        NSLog(@"productNum %@ already exist in DB index = %ld", productNum, (long)indexInDB);
        return NO;
    }
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        NSString *insertSQL = [NSString stringWithFormat:@"INSERT INTO products (productNum, targetStock, selectedCountry) values (\"%@\",%ld,\"%@\")", productNum, targetStock, selectedCountryParam];

        const char *insert_stmt = [insertSQL UTF8String];
        NSInteger result = sqlite3_prepare_v2(database, insert_stmt, -1, &statement, NULL);
        NSInteger sqlite3_step_statement = sqlite3_step(statement);
        
        if (sqlite3_step_statement == SQLITE_DONE) {
            sqlite3_reset(statement);
            return YES;
        } else {
            sqlite3_reset(statement);
            return NO;
        }
    }
    return NO;
}

/////////////////////////////   deleteProductFromDB   /////////////////////////////////////
- (BOOL) deleteProductFromDB:(NSString*)productNum {
    NSLog(@"deleteProductFromDB(%@)", productNum);
    
    const char *dbpath = [databasePath UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK) {
        NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM products WHERE productNum =%@", productNum];
        
        const char *delete_stmt = [deleteSQL UTF8String];
        sqlite3_prepare_v2(database, delete_stmt, -1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            sqlite3_finalize(statement);
            sqlite3_close(database);
            return YES;
        } else {
            sqlite3_finalize(statement);
            sqlite3_close(database);
            return NO;
        }
    }
    return NO;
}

/////////////////////////////   clearProductDB   /////////////////////////////////////
- (void) clearProductDB{
    NSLog(@"clearProductDB");
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent: @"TrackingProduct.db"]];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:databasePath]){
        [[NSFileManager defaultManager] removeItemAtPath:databasePath error:nil];
    }
    sharedInstance = nil;
    database = nil;
    //sqlite3_reset(statement);
}

@end
